# Generated by Django 5.0 on 2023-12-13 23:51

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0004_alter_todoitem_todo_list"),
    ]

    operations = [
        migrations.RenameField(
            model_name="todoitem",
            old_name="todo_list",
            new_name="list",
        ),
    ]
