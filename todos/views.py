from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from .forms import TodoListForm

# Create your views here.
def todo_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todos_list": todo_lists,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    todo_list_detail = TodoList.objects.get(id=id)
    context = {
        "todo_list_detail": todo_list_detail,
    }
    return render(request, "todos/details.html", context)

def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.id)

    else:
        form = TodoListForm()

    context = {
        "form_key": form,
        }
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)

    context = {
        "form": form
    }

    return render(request, "todos/edit.html", context)
